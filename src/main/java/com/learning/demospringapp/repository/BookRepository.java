package com.learning.demospringapp.repository;

import com.learning.demospringapp.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {
}
