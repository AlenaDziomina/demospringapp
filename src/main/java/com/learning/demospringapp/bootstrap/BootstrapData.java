package com.learning.demospringapp.bootstrap;

import com.learning.demospringapp.model.Author;
import com.learning.demospringapp.model.Book;
import com.learning.demospringapp.model.Publisher;
import com.learning.demospringapp.repository.AuthorRepository;
import com.learning.demospringapp.repository.BookRepository;
import com.learning.demospringapp.repository.PublisherRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Component
@RequiredArgsConstructor
public class BootstrapData implements CommandLineRunner {

    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;
    private final PublisherRepository publisherRepository;

    @Override
    public void run(String... args) throws Exception {
        Author author = Author.builder().firstName("John").lastName("Tolkien").build();
        Publisher publisher = Publisher.builder().name("AST publisher").build();
        Book book1 = Book.builder().name("Hobbits").publishYear(2001)
                .authors(Collections.singleton(author))
                .bookPublisher(publisher)
                .build();
        Book book2 = Book.builder().name("The Lord of the Rings").publishYear(2003)
                .authors(Collections.singleton(author))
                .bookPublisher(publisher)
                .build();


        Set<Book> bookSet = new HashSet<>();
        bookSet.add(book1);
        bookSet.add(book2);
        author.setBooks(bookSet);

        publisher.setBooks(bookSet);

        authorRepository.save(author);
        publisherRepository.save(publisher);
        bookRepository.save(book1);
        bookRepository.save(book2);


        log.info("Author count in DB: {}", authorRepository.count());
        log.info("Book count in DB: {}", bookRepository.count());
        log.info("Publisher count in DB: {}", publisherRepository.count());

    }
}
